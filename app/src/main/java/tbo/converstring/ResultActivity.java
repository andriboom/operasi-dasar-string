package tbo.converstring;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import tbo.converstring.assets.ConvertString;
import tbo.converstring.assets.model.ModelConvertString;

public class ResultActivity extends AppCompatActivity {

    @BindView(R.id.result_prefix)
    LinearLayout resultPrefix;
    @BindView(R.id.ll_prefix)
    LinearLayout llPrefix;
    @BindView(R.id.result_properprefix)
    LinearLayout resultProperprefix;
    @BindView(R.id.ll_properprefix)
    LinearLayout llProperprefix;
    @BindView(R.id.result_postfix)
    LinearLayout resultPostfix;
    @BindView(R.id.ll_postfix)
    LinearLayout llPostfix;
    @BindView(R.id.result_properpostfix)
    LinearLayout resultProperpostfix;
    @BindView(R.id.ll_properpostfix)
    LinearLayout llProperpostfix;
    ProgressDialog loading;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        ButterKnife.bind(this);
        context = this;
        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        loadData();
    }

    private void loadData() {
        Intent intentData = getIntent();
        String query = intentData.getStringExtra("query");
        if (TextUtils.isEmpty(query)){
            finish();
            Toast.makeText(context, "Oops something error.", Toast.LENGTH_SHORT).show();
        }else{
            loading = ProgressDialog.show(context,null,"Memproses String",true,false);
            //process prefix
            inputIntoLayout(resultPrefix,ConvertString.convertPrefix(query));
            //process properprefix
            inputIntoLayout(resultProperprefix,ConvertString.convertProperPrefix(query));
            //process prostfix
            inputIntoLayout(resultPostfix,ConvertString.convertPostfix(query));
            //process properpostfix
            inputIntoLayout(resultProperpostfix,ConvertString.convertProperPostfix(query));
            loading.dismiss();
        }
    }

    private void inputIntoLayout(LinearLayout layout,List<ModelConvertString> result) {
        for (ModelConvertString data: result){
            View prefix_add = getLayoutInflater().inflate(R.layout.item_result, layout,false);
            TextView tvNamePrefix = prefix_add.findViewById(R.id.tv_name);
            TextView tvResultPrefix = prefix_add.findViewById(R.id.tv_result);
            tvNamePrefix.setText(data.getName());
            tvResultPrefix.setText(data.getResult());
            layout.addView(prefix_add);
        }
    }

    @OnClick({R.id.ll_prefix, R.id.ll_properprefix, R.id.ll_postfix, R.id.ll_properpostfix})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_prefix:

                hiddingorshowresult(resultPrefix);
                break;
            case R.id.ll_properprefix:
                hiddingorshowresult(resultProperprefix);
                break;
            case R.id.ll_postfix:
                hiddingorshowresult(resultPostfix);
                break;
            case R.id.ll_properpostfix:
                hiddingorshowresult(resultProperpostfix);
                break;
        }
    }

    private void hiddingorshowresult(LinearLayout layout) {
        if (layout.getVisibility()==View.VISIBLE){
//            Toast.makeText(context, "gone", Toast.LENGTH_SHORT).show();
            layout.setVisibility(View.GONE);
        }else{
//            Toast.makeText(context, "visible", Toast.LENGTH_SHORT).show();
            layout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
