package tbo.converstring.assets;

import java.util.ArrayList;
import java.util.List;

import tbo.converstring.assets.model.ModelConvertString;

public class ConvertString {


    public static List<ModelConvertString> convertPrefix(String q){
        List<ModelConvertString> output = new ArrayList<>();
        String[] separted = q.split(" ");
        for (String s : separted) {
            int stringLeng = s.length();
            String tempString="";
            for (int j = 0; j < s.length(); j++) {
                tempString += s.substring(0,stringLeng)+" \n";
                stringLeng--;
            }
            tempString +="\u03B5";
            output.add(new ModelConvertString(s, tempString));
        }
        return output;
    }

    public static List<ModelConvertString> convertProperPrefix(String q){
        List<ModelConvertString> output = new ArrayList<>();
        String[] separted = q.split(" ");
        for (String s : separted) {
            int stringLeng = s.length()-1;
            String tempString="";
            for (int j = 0; j < s.length()-1; j++) {
                tempString += s.substring(0,stringLeng)+" \n";
                stringLeng--;
            }
            tempString +="\u03B5";
            output.add(new ModelConvertString(s, tempString));
        }
        return output;
    }

    public static List<ModelConvertString> convertPostfix(String q){
        List<ModelConvertString> output = new ArrayList<>();
        String[] separted = q.split(" ");
        for (String s : separted) {
            int stringLeng = s.length();
            String tempString = "";
            for (int j = 0; j < s.length(); j++) {
                tempString += s.substring(j,stringLeng)+" \n";
            }
            tempString +="\u03B5";
            output.add(new ModelConvertString(s, tempString));
        }
        return output;
    }

    public static List<ModelConvertString> convertProperPostfix(String q){
        List<ModelConvertString> output = new ArrayList<>();
        String[] separted = q.split(" ");
        for (String s : separted) {
            int stringLeng = s.length();
            String tempString = "";
            for (int j = 1; j < s.length(); j++) {
                tempString += s.substring(j,stringLeng)+" \n";
            }
            tempString +="\u03B5";
            output.add(new ModelConvertString(s, tempString));
        }
        return output;
    }




}
