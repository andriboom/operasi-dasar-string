package tbo.converstring;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    private Context context;
    @BindView(R.id.edt_query)
    EditText edtQuery;
    @BindView(R.id.btn_convert)
    Button btnConvert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        context = this;
    }

    @OnClick(R.id.btn_convert)
    public void onViewClicked() {
        edtQuery.setTextColor(Color.RED);
        if (edtQuery.getText().toString().isEmpty()){
            edtQuery.setError("Harus di isi");
            edtQuery.requestFocus();
        }else{
            edtQuery.setError(null);
            Intent intent = new Intent(context, ResultActivity.class);
            intent.putExtra("query",edtQuery.getText().toString());
            startActivity(intent);
        }
    }
}
